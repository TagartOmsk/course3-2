package ru.omsu.imit.socketserver;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class NameRandomizer {
    private static List<String> names = Arrays.asList("Alaina", "Jaylan", "Laila", "Emilie", "Aditya", "Leslie", "Samson", "Marely", "Bentley",
            "Remington", "Audrina", "Julio", "Remington", "Sienna", "Bryanna", "Clarissa", "Athena", "Jaquan", "Rigoberto",
            "Camila");
    private static List<String> lastNames = Arrays.asList("Tapia", "Conway", "Bean", "Guerrero", "Roman", "Mccullough",
            "Blanchard", "Davis", "Melton", "Frost", "Carpenter", "Vaughan", "Jacobson", "Glover", "Ho", "Miles", "Page",
            "Cain", "Le", "Quinn");

    private static Random rand = new Random();

    public static String generateName(){
        return names.get(rand.nextInt(names.size()));
    }

    public static String generateLastName(){
        return lastNames.get(rand.nextInt(lastNames.size()));
    }
}
