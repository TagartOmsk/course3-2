package ru.omsu.imit.socketserver;

public class Error extends Throwable {
    private String errorString;

    public Error(String errorString) {
        this.errorString = errorString;
    }

    public String getErrorString() {
        return errorString;
    }
}
