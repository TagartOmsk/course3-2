package ru.omsu.imit.socketserver;

import com.google.gson.Gson;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Client {
    private static Random random = new Random();
    private static Gson json = new Gson();
    private final int port;
    private final String address;

    public Client(int port, String address) {
        this.port = port;
        this.address = address;
    }

    /*public void add(Person person) {
        String res = server.add(json.toJson(person));
        if (!res.equals(json.toJson(RequestState.getRequestSuccess()))) {
            System.out.println("an error occured: " + json.fromJson(res, Error.class).getErrorString());
        } else {
            System.out.println(person.toString() + " added successfully");
        }
    }

    public void add(String jsonRequest) {
        add(json.fromJson(jsonRequest, Person.class));
    }

    public void get(String request) {
        String res = server.get(request);
        if (res.equals(json.toJson(RequestState.getRequestSuccess()))) {
            System.out.println("Specified person not found");
        } else {
            System.out.println("Found " + json.fromJson(res, Person.class).toString());
        }
    }*/

    public void mainRandom(int attempts, boolean exit) {

        InetAddress ipAddress;
        try {
            ipAddress = InetAddress.getByName(address);
        } catch (UnknownHostException e) {
            return;
        }

        try (Socket socket = new Socket(ipAddress, port);
             DataInputStream in = new DataInputStream(socket.getInputStream());
             DataOutputStream out = new DataOutputStream(socket.getOutputStream());
             BufferedReader keyboardReader = new BufferedReader(new InputStreamReader(System.in, "CP866"))) {

            System.out.println(socket.getLocalPort());
            String prefix = null;

            Person person = null;
            for (int i = 0; i < attempts; i++) {
                if (exit) {
                    out.writeUTF("quit");
                    out.flush();
                }
                if (random.nextBoolean()) {
                    prefix = "add ";
                } else {
                    prefix = "get ";
                }
                person = new Person(NameRandomizer.generateName(), NameRandomizer.generateLastName(), random.nextInt(70));

                out.writeUTF(prefix + json.toJson(person));
                out.flush();
                System.out.println(prefix + in.readUTF());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<String> executeRequests(String... requests) {
//        final String address = this.address;
        InetAddress ipAddress;
        try {
            ipAddress = InetAddress.getByName(address);
        } catch (UnknownHostException e) {
            return null;
        }

        try (Socket socket = new Socket(ipAddress, this.port);
             DataInputStream in = new DataInputStream(socket.getInputStream());
             DataOutputStream out = new DataOutputStream(socket.getOutputStream());
             BufferedReader keyboardReader = new BufferedReader(new InputStreamReader(System.in, "CP866"))) {
            List<String> res = new ArrayList<>(requests.length);
            for (String request : requests) {
                out.writeUTF(request);
                out.flush();
                res.add(in.readUTF());
            }
            return res;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args) {
        final int port = 9000;
        final String address = "localhost";
        InetAddress ipAddress;
        try {
            ipAddress = InetAddress.getByName(address);
        } catch (UnknownHostException e) {
            return;
        }
        try (Socket socket = new Socket(ipAddress, port);
             DataInputStream in = new DataInputStream(socket.getInputStream());
             DataOutputStream out = new DataOutputStream(socket.getOutputStream());
             BufferedReader keyboardReader = new BufferedReader(new InputStreamReader(System.in, "CP866"))) {

            System.out.println(socket.getLocalPort());
            String prefix = null;

            Person person = null;
            while (true) {
                if (random.nextInt(20) == 0) {
                    System.out.println("Print '1' to shutdown the server:");
                    int switchOff = keyboardReader.read();
                    if (switchOff == 49) {
                        out.writeUTF("quit");
                        out.flush();
                    }
                    break;
                }
                if (random.nextBoolean()) {
                    prefix = "add ";
                } else {
                    prefix = "get ";
                }
                person = new Person(NameRandomizer.generateName(), NameRandomizer.generateLastName(), random.nextInt(70));

                out.writeUTF(prefix + json.toJson(person));
                out.flush();
                System.out.println(prefix + in.readUTF());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
