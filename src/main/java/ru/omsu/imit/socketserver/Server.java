package ru.omsu.imit.socketserver;

import com.google.gson.Gson;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {
    private List<Person> people;
    private static Gson json = new Gson();

    public Server() {
        people = new ArrayList<>(5);
        for (int i = 0; i < 5; i++) {
            people.add(new Person(NameRandomizer.generateName(), NameRandomizer.generateName(), 20 + i * 6));
        }
    }

    public Server(List<Person> people) {
        this.people = people;
    }

    public String get(String request) {
        PersonRequest temp = json.fromJson(request, PersonRequest.class);
        for (Person man : people) {
            if (temp.getFirstName() != null && !temp.getFirstName().equals(man.getFirstName())) continue;
            if (temp.getLastName() != null && !temp.getLastName().equals(man.getLastName())) continue;
            if (temp.getAge() != null && !temp.getAge().equals(man.getAge())) continue;
            return json.toJson(new PersonRequest(man.getFirstName(), man.getLastName(), man.getAge()));
        }
        return json.toJson(new Error("Not found"));
    }

    public String add(String request) {
        PersonRequest person = json.fromJson(request, PersonRequest.class);
        if (!isValid(person)) {
            return json.toJson(new Error("Invalid data"));
        }
        people.add(requestToPerson(person));
        return json.toJson(RequestState.getRequestSuccess());
    }

    private boolean isValid(PersonRequest person) {
        return person.getFirstName() != null && person.getLastName() != null && person.getAge() != null;
    }

    private Person requestToPerson(PersonRequest request) {
        return new Person(request.getFirstName(), request.getLastName(), request.getAge());
    }

    public void showAll() {
        for (Person person : people) {
            System.out.println(person.toString());
        }
    }

    public static void main(String[] args) {
        Server server = new Server();
        final int port = 9000;
        boolean exit = false;
        while (!exit) {
            try (ServerSocket serverSocket = new ServerSocket(port);
                 Socket socket = serverSocket.accept();
                 DataInputStream in = new DataInputStream(socket.getInputStream());
                 DataOutputStream out = new DataOutputStream(socket.getOutputStream())) {
                System.out.printf("Listening on port %d\n", port);

                while (true) {
                    String clientCommand = in.readUTF();
                    if (clientCommand.equalsIgnoreCase("quit")) {
                        out.writeUTF("{}");
                        out.flush();
                        System.out.println("Stopping\n");
                        server.showAll();
                        exit = true;
                        break;
                    }
                    if (clientCommand.startsWith("add")) {
                        out.writeUTF(server.add(clientCommand.substring(4)));
                        out.flush();
                    } else {
                        if (clientCommand.startsWith("get")) {
                            out.writeUTF(server.get(clientCommand.substring(4)));
                            out.flush();
                        } else {
                            out.writeUTF(json.toJson(new Error("Unknown command")));
                            out.flush();
                        }
                    }
                }
            } catch (EOFException e) {
                System.out.println("AuctionClient disconnected, waiting for another.");

            } catch (IOException x) {
                System.out.println("An error occured, please restart the server");
            }
        }
    }
}
