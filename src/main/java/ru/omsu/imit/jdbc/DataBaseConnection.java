package ru.omsu.imit.jdbc;

import java.sql.*;

public class DataBaseConnection {

    private static boolean loadDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return true;
        } catch (ClassNotFoundException e) {
            System.out.println("Error loading JDBC Driver ");
            e.printStackTrace();
            return false;
        }
    }

    public static void visualizeBooks(ResultSet rs) {
        try {
            while (rs.next()) {
                System.out.printf("%30s %4d %7d\n", rs.getString("title"),
                        rs.getInt("year"), rs.getInt("pages"));
            }
            System.out.println();
            rs.close();
        } catch (SQLException e) {
            System.out.println("Invalid query, try again");
            e.printStackTrace();
        }
    }

    public static void visualizeBooksExtended(ResultSet rs) {
        try {

            while (rs.next()) {
                System.out.printf("%30s %4d %7d %20s %20s\n", rs.getString("title"),
                        rs.getInt("year"), rs.getInt("pages"),
                        rs.getString("publishing_house"), rs.getString("binding"));
            }
            System.out.println();
            rs.close();
        } catch (SQLException e) {
            System.out.println("Invalid query, try again");
            e.printStackTrace();
        }
    }

    public static ResultSet select(String query, Connection con, Object... params) {
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            for (int i = 0; i < params.length; i++) {
                if (params[i] instanceof Integer) {
                    stmt.setInt(i + 1, (int) params[i]);
                } else {
                    if (params[i] instanceof String) {
                        stmt.setString(i + 1, (String) params[i]);
                    } else {
                        throw new RuntimeException("Wrong type");
                    }
                }
            }
            return stmt.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Invalid query, try again");
        }
    }

    public static void visualizeBooksExtendedAltered(ResultSet rs) {
        try {

            while (rs.next()) {
                System.out.printf("%30s %4d %7d %20s %20s\n", rs.getString("bookname"),
                        rs.getInt("year"), rs.getInt("pages"),
                        rs.getString("publishing_house"), rs.getString("binding"));
            }
            System.out.println();
            rs.close();
        } catch (SQLException e) {
            System.out.println("Invalid query, try again");
            e.printStackTrace();
        }
    }

    public static void updateBooks(String query, Connection con) {
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Invalid query, try again");
            e.printStackTrace();
        }
    }

    public static void updateBooks(String query, Connection con, Object... params) {
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            for (int i = 0; i < params.length; i++) {
                if (params[i] instanceof Integer) {
                    stmt.setInt(i + 1, (int) params[i]);
                } else {
                    if (params[i] instanceof String) {
                        stmt.setString(i + 1, (String) params[i]);
                    } else {
                        throw new RuntimeException("Wrong type");
                    }
                }
            }
            stmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Invalid query, try again");
            e.printStackTrace();
        }
    }

    private static void reCreateBooks(Connection con) {
        updateBooks("drop table if exists books", con);
        updateBooks("create table books(id int not null auto_increment primary key, title varchar(30), `year` " +
                "smallint, pages int)", con);
        updateBooks("insert into books values(null, 'The Flowers of Evil',1868, 485);", con);
        updateBooks("insert into books values(null, 'Divine Comedy', 1320, 760);", con);
        updateBooks("insert into books values(null, 'Faust', 1828, 416);", con);
        updateBooks("insert into books values(null, 'Roadside picnic', 1972, 147);", con);
        updateBooks("insert into books values(null, 'Quietly Flows the Don', 1928, 1679);", con);
        updateBooks("insert into books values(null, 'Mein Kampf', 1926, 357);", con);
    }

    public static void main(String[] args) {
        if (!loadDriver())
            return;
        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost/bookstore?user=root&password=overlord25")) {
            reCreateBooks(con);
            visualizeBooks(select("select * from books", con));
//            selectBooks("select * from books limit 3", con);
//            selectBooks("select * from books where id = 2", con);
//            selectBooks("select * from books where title = \"Flowers of Evil\" or title =  \"Roadside Picnic\"", con);
//            selectBooks("select * from books where title like \"d%\"", con);
//            selectBooks("select * from books where title like \"%l\"", con);
//            selectBooks("select * from books where year between 1500 and 1900", con);
//            selectBooks("select * from books where pages between 300 and 500", con);
//            selectBooks("select * from books where year between 1500 and 1900 and pages between 300 and 500", con);

            updateBooks("update books set pages = ?", con, 256);
            visualizeBooks(select("select * from books", con));
            reCreateBooks(con);
            updateBooks("update books set pages = ? where pages >= ?;", con, 700, 700);
            visualizeBooks(select("select * from books", con));

//            updateBooks("update books set pages = 1000 where pages >= 1000;", con);
//            selectBooks("select * from books", con);
//            updateBooks("update books set pages = 1000 where pages >= 700 and `year` < 1900;", con);
//            selectBooks("select * from books", con);
//            reCreateBooks(con);
//            updateBooks("update books set pages = 1000 where pages >= 700 and title like \"D%\";", con);
//            selectBooks("select * from books", con);
//            reCreateBooks(con);
//
//            updateBooks("delete from books where pages > 500;", con);
//            selectBooks("select * from books", con);
//            reCreateBooks(con);
//            updateBooks("delete from books where title like \"D%\";", con);
//            selectBooks("select * from books", con);
//            reCreateBooks(con);
//            updateBooks("delete from books where id between 2 and 5;", con);
//            selectBooks("select * from books", con);
//            reCreateBooks(con);
//            updateBooks("delete from books where title like \"Roadside picnic\";", con);
//            selectBooks("select * from books", con);
//            reCreateBooks(con);
//            updateBooks("delete from books where title like \"Divine comedy\" or title like \"Faust\" or title like \"The Flowers of Evil\";", con);
//            selectBooks("select * from books", con);
//            reCreateBooks(con);
//            updateBooks("delete from books;", con);
//            selectBooks("select * from books", con);
//            reCreateBooks(con);
//
//            updateBooks("alter table books add publishing_house varchar(50);", con);
//            updateBooks("alter table books add binding enum( 'no binding', 'soft', 'hard') default 'no binding';", con);
//            updateBooks("update books set publishing_house = 'BuKa' where id between 1 and 6;", con);
//            selectBooksExtended("select * from books", con);
//            updateBooks("alter table books change title bookname varchar(30);", con);
//            visualizeBooksExtendedAltered("select * from books", con);
//            updateBooks("rename table books to book_potato;", con);
//            visualizeBooksExtendedAltered("select * from book_potato;", con);
//            updateBooks("drop table book_potato;", con);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
