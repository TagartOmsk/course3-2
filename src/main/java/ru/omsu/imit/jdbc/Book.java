package ru.omsu.imit.jdbc;

public class Book {
    private String title;
    private int year;
    private int pages;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public Book(String title, int year, int pages) {
        if (title.isEmpty()) throw new RuntimeException("Empty title");
        this.title = title;
        if (year < 0 ) throw new RuntimeException("Year can't be less than zero, you're probably wrong");
        this.year = year;
        if (pages < 1 ) throw new RuntimeException("There can't be less than one page, you're probably wrong");
        this.pages = pages;
    }
}
