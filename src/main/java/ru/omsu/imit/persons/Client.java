package ru.omsu.imit.persons;

import com.google.gson.Gson;

public class Client {
    private Server server;
    private static Gson json = new Gson();

    public Client(Server server) {
        this.server = server;
    }

    public void add(Person person) {
        String res = server.add(json.toJson(person));
        if (!res.equals(json.toJson(RequestState.getRequestSuccess()))) {
            System.out.println("an error occured: " + json.fromJson(res, Error.class).getErrorString());
        } else {
            System.out.println(person.toString() + " added successfully");
        }
    }

    public void add(String jsonRequest) {
        add(json.fromJson(jsonRequest, Person.class));
    }

    public void get(String request) {
        String res = server.get(request);
        if (res.equals(json.toJson(RequestState.getRequestSuccess()))) {
            System.out.println("Specified person not found");
        } else {
            System.out.println("Found " + json.fromJson(res, Person.class).toString());
        }
    }
}
