package ru.omsu.imit.persons;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class Server {
    private List<Person> people;
    private static Gson json = new Gson();

    public Server() {
        people = new ArrayList<>(5);
        for (int i = 0; i < 5; i++) {
            people.add(new Person("Искандер", "Сухомельнов", 20 + i * 6));
        }
    }

    public Server(List<Person> people) {
        this.people = people;
    }

    public String get(String request) {
        PersonRequest temp = json.fromJson(request, PersonRequest.class);
        for (Person man : people) {
            if (temp.getFirstName() != null && !temp.getFirstName().equals(man.getFirstName())) continue;
            if (temp.getLastName() != null && !temp.getLastName().equals(man.getLastName())) continue;
            if (temp.getAge() != null && !temp.getAge().equals(man.getAge())) continue;
            return json.toJson(new PersonRequest(man.getFirstName(), man.getLastName(), man.getAge()));
        }
        return json.toJson(RequestState.getRequestFail());

        // return json.toJson(people.stream().filter(x -> x.equals(temp)).findFirst().orElse(new Person(null,null,null)));
    }

    public String add(String request) {
        PersonRequest person = json.fromJson(request, PersonRequest.class);
        if (!isValid(person)) {
            return json.toJson(new Error("Invalid data"));
        }
        people.add(requestToPerson(person));
        return json.toJson(RequestState.getRequestSuccess());
    }

    private boolean isValid(PersonRequest person) {
        return person.getFirstName() != null && person.getLastName() != null && person.getAge() != null;
    }

    private Person requestToPerson(PersonRequest request) {
        return new Person(request.getFirstName(), request.getLastName(), request.getAge());
    }
}
