package ru.omsu.imit.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class Client {
    private static Random random = new Random();
    private static Gson json = new Gson();
    private final int port;
    private final String address;

    private static final Gson GSON = new GsonBuilder().create();
    private static javax.ws.rs.client.Client createClient() {
        return ClientBuilder.newClient();
    }

    public Client(int port, String address) {
        this.port = port;
        this.address = address;
    }

    public Object get(String url, Class<?> classResponse) {
        javax.ws.rs.client.Client client = createClient();
        WebTarget myResource = client.target(url);
        Invocation.Builder builder = myResource.request(MediaType.APPLICATION_JSON);
        Response response = builder.get();
        String body = response.readEntity(String.class);
        Object obj = GSON.fromJson(body, classResponse);
        client.close();
        return obj;
    }

    public Object post(String url, Object object, Class<?> classResponse) {
        javax.ws.rs.client.Client client = createClient();
        WebTarget myResource = client.target(url);
        Invocation.Builder builder = myResource.request(MediaType.APPLICATION_JSON);
        Response response = builder.post(Entity.json(object));
        String body = response.readEntity(String.class);
        Object obj = GSON.fromJson(body, classResponse);
        client.close();
        return obj;
    }

//    public List<String> executeRequests(String... requests) {
//        final int serverPort = 9000;
//        final String address = "localhost";
//        InetAddress ipAddress;
//        try {
//            ipAddress = InetAddress.getByName(address);
//        } catch (UnknownHostException e) {
//            return null;
//        }
//
//        try (Socket socket = new Socket(ipAddress, serverPort);
//             DataInputStream in = new DataInputStream(socket.getInputStream());
//             DataOutputStream out = new DataOutputStream(socket.getOutputStream());
//             BufferedReader keyboardReader = new BufferedReader(new InputStreamReader(System.in, "CP866"))) {
//            List<String> res = new ArrayList<>(requests.length);
//            for (String request : requests) {
//                out.writeUTF(request);
//                out.flush();
//                res.add(in.readUTF());
//            }
//            return res;
//        } catch (IOException e) {
//            e.printStackTrace();
//            return null;
//        }
//    }

//    public static void main(String[] args) {
//        final int serverPort = 9000;
//        final String address = "localhost";
//        InetAddress ipAddress;
//        try {
//            ipAddress = InetAddress.getByName(address);
//        } catch (UnknownHostException e) {
//            return;
//        }
//        try (Socket socket = new Socket(ipAddress, serverPort);
//             DataInputStream in = new DataInputStream(socket.getInputStream());
//             DataOutputStream out = new DataOutputStream(socket.getOutputStream());
//             BufferedReader keyboardReader = new BufferedReader(new InputStreamReader(System.in, "CP866"))) {
//
//            System.out.println(socket.getLocalPort());
//            String prefix = null;
//
//            Person person = null;
//            while (true) {
//                if (random.nextInt(20) == 0) {
//                    System.out.println("Print '1' to shutdown the server:");
//                    int switchOff = keyboardReader.read();
//                    if (switchOff == 49) {
//                        out.writeUTF("quit");
//                        out.flush();
//                    }
//                    break;
//                }
//                if (random.nextBoolean()) {
//                    prefix = "add ";
//                } else {
//                    prefix = "get ";
//                }
//                person = new Person(NameRandomizer.generateName(), NameRandomizer.generateLastName(), random.nextInt(70));
//
//                out.writeUTF(prefix + json.toJson(person));
//                out.flush();
//                System.out.println(prefix + in.readUTF());
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
}
