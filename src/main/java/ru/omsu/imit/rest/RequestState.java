package ru.omsu.imit.rest;

public class RequestState {
    private static RequestState self;
    // класс-пустышка для получения пустого json'a

    private RequestState() {
        self = this;
    }

    public static RequestState getRequestSuccess() {
        if(self == null) self = new RequestState();
        return self;
    }

    public static RequestState getRequestFail(){
        return getRequestSuccess();
    }

}
