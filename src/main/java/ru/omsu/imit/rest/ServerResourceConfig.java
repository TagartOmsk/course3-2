package ru.omsu.imit.rest;

import org.glassfish.jersey.server.ResourceConfig;

public class ServerResourceConfig extends ResourceConfig {
    public ServerResourceConfig(){
        packages("ru.omsu.imit.rest");
    }
}
