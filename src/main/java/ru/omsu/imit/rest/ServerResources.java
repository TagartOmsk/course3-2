package ru.omsu.imit.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api")
public class ServerResources {

    private static DatabaseEmulator server = new DatabaseEmulator();

    private static final Gson GSON = new GsonBuilder().create();

    @POST
    @Path("/people")
    @Consumes("application/json")
    @Produces("application/json")
    public Response post(String json){
        String response = server.post(json);
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/people")
    @Produces("application/json")
    public Response get(@QueryParam("firstName") String firstName,
                    @QueryParam("lastName") String lastName,
                    @QueryParam("age") Integer age){
        String response = GSON.toJson(server.get(firstName, lastName, age));
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }
}
