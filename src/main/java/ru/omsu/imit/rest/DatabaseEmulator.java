package ru.omsu.imit.rest;

import com.google.gson.Gson;
import com.sun.scenario.Settings;
import org.eclipse.jetty.server.Server;
import org.glassfish.jersey.jetty.JettyHttpContainerFactory;

import javax.ws.rs.core.UriBuilder;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DatabaseEmulator {
    private List<Person> people;
    private static Gson json = new Gson();
    private final static int port = 9000;

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseEmulator.class);

    private static Server jettyServer;

    private static void attachShutDownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                stopServer();
            }
        });
    }

    public static void createServer() {
        URI baseUri = UriBuilder.fromUri("http://localhost/").port(port).build();
        ServerResourceConfig config = new ServerResourceConfig();
        jettyServer = JettyHttpContainerFactory.createServer(baseUri, config);
        LOGGER.info("Server started at port " + port);
    }

    public static void stopServer() {
        LOGGER.info("Stopping server");
        try {
            jettyServer.stop();
            jettyServer.destroy();
        } catch (Exception e) {
            LOGGER.error("Error stopping service", e);
            System.exit(1);
        }
        LOGGER.info("Server stopped");
    }


    public static void main(String[] args) {
        attachShutDownHook();
        createServer();
    }

    public DatabaseEmulator() {
        people = new ArrayList<>(5);
        for (int i = 0; i < 5; i++) {
            people.add(new Person(NameRandomizer.generateName(), NameRandomizer.generateLastName(), 20 + i * 6));
        }
    }

    public DatabaseEmulator(List<Person> people) {
        this.people = people;
    }

    public String get(String firstName, String lastName, Integer age) {
        for (Person man : people) {
            if (firstName != null && !firstName.equals(man.getFirstName())) continue;
            if (lastName != null && !lastName.equals(man.getLastName())) continue;
            if (age != null && !age.equals(man.getAge())) continue;
            return json.toJson(new PersonRequest(man.getFirstName(), man.getLastName(), man.getAge()));
        }
        return json.toJson(new Error("Not found"));
    }

    public String post(String request) {
        PersonRequest person = json.fromJson(request, PersonRequest.class);
        if (!isValid(person)) {
            return json.toJson(new Error("Invalid data"));
        }
        people.add(requestToPerson(person));
        return json.toJson(RequestState.getRequestSuccess());
    }

    private boolean isValid(PersonRequest person) {
        return person.getFirstName() != null && person.getLastName() != null && person.getAge() != null;
    }

    private Person requestToPerson(PersonRequest request) {
        return new Person(request.getFirstName(), request.getLastName(), request.getAge());
    }

    public void showAll() {
        for (Person person : people) {
            System.out.println(person.toString());
        }
    }

//    public static void main(String[] args) {
//        DatabaseEmulator server = new DatabaseEmulator();
//        final int port = 9000;
//        while (true) {
//            try (ServerSocket serverSocket = new ServerSocket(port);
//                 Socket socket = serverSocket.accept();
//                 DataInputStream in = new DataInputStream(socket.getInputStream());
//                 DataOutputStream out = new DataOutputStream(socket.getOutputStream())) {
//                System.out.printf("Listening on port %d\n", port);
//
//                while (true) {
//                    String clientCommand = in.readUTF();
//                    if (clientCommand.equalsIgnoreCase("quit")) {
//                        System.out.println("Stopping\n");
//                        server.showAll();
//                        return;
//                    }
//                    if (clientCommand.startsWith("add")) {
//                        out.writeUTF(server.post(clientCommand.substring(4)));
//                        out.flush();
//                    } else {
//                        if (clientCommand.startsWith("get")) {
//                            out.writeUTF(server.get(clientCommand.substring(4)));
//                            out.flush();
//                        } else {
//                            out.writeUTF(json.toJson(new Error("Unknown command")));
//                            out.flush();
//                        }
//                    }
//                }
//            } catch (EOFException e) {
//                System.out.println("AuctionClient disconnected, waiting for another.");
//
//            } catch (IOException x) {
//                System.out.println("An error occured, please restart the server");
//            }
//        }
//    }
}
