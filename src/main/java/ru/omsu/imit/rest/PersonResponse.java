package ru.omsu.imit.rest;

import java.util.ArrayList;

public class PersonResponse {

    private String result;

    public PersonResponse(String str) {
       this.result = str;
    }

    public String getResult() {
        return result;
    }
}
