package ru.omsu.imit.auction;

import org.eclipse.jetty.server.Server;
import org.glassfish.jersey.jetty.JettyHttpContainerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.auction.config.AuctionResourceConfig;
import ru.omsu.imit.auction.utils.MyBatisUtils;
import ru.omsu.imit.rest.ServerResourceConfig;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;

public class ServerStarter {
    private final static int port = 9000;

    private static final Logger LOGGER = LoggerFactory.getLogger(ServerStarter.class);

    private static Server jettyServer;

    public static int getPort(){
        return port;
    }

    public static void attachShutDownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                stopServer();
            }
        });
    }

    public static void createServer() {
        URI baseUri = UriBuilder.fromUri("http://localhost/").port(port).build();
        AuctionResourceConfig config = new AuctionResourceConfig();
        MyBatisUtils.initSqlSessionFactory();
        jettyServer = JettyHttpContainerFactory.createServer(baseUri, config);
        LOGGER.info("Server started at port " + port);
    }

    public static void stopServer() {
        LOGGER.info("Stopping server");
        try {
            jettyServer.stop();
            jettyServer.destroy();
        } catch (Exception e) {
            LOGGER.error("Error stopping service", e);
            System.exit(1);
        }
        LOGGER.info("Server stopped");
    }


    public static void main(String[] args) {
        attachShutDownHook();
        createServer();
    }
}
