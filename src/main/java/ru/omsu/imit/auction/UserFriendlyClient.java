package ru.omsu.imit.auction;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import ru.omsu.imit.auction.dto.response.BidResponse;
import ru.omsu.imit.auction.dto.response.CategoryResponse;
import ru.omsu.imit.auction.dto.response.EmptyResponse;
import ru.omsu.imit.auction.dto.response.ItemResponse;
import ru.omsu.imit.auction.exceptions.AuctionException;
import ru.omsu.imit.auction.model.Bid;
import ru.omsu.imit.auction.model.Category;
import ru.omsu.imit.auction.model.Item;
import ru.omsu.imit.auction.services.Utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class UserFriendlyClient {
    static Gson GSON = new Gson();
    static AuctionClient client = new AuctionClient();
    static String ADDRESS = "http://localhost:" + ServerStarter.getPort() + "/auction";
    static Type EMPTY_RESPONSE_TYPE = new TypeToken<EmptyResponse>() {
    }.getType();
    static Type CATEGORY_RESPONSE_TYPE = new TypeToken<CategoryResponse>() {
    }.getType();
    static Type ITEM_RESPONSE_TYPE = new TypeToken<ItemResponse>() {
    }.getType();
    static Type BID_RESPONSE_TYPE = new TypeToken<BidResponse>() {
    }.getType();
    static Type CATEGORY_RESPONSE_LIST_TYPE = new TypeToken<List<CategoryResponse>>() {
    }.getType();
    static Type ITEM_RESPONSE_LIST_TYPE = new TypeToken<List<ItemResponse>>() {
    }.getType();
    static Type BID_RESPONSE_LIST_TYPE = new TypeToken<List<BidResponse>>() {
    }.getType();

    /*ADD*/

    public static Category addCategory(Category category) throws AuctionException {
        Object response = client.post(ADDRESS + "/cat/post", category, CATEGORY_RESPONSE_TYPE);
        if (!(response instanceof CategoryResponse)) throw (AuctionException) response;
        return Utils.responseToCategory((CategoryResponse) response);
    }

    public static Item addItem(Item item) throws AuctionException {
        Object response = client.post(ADDRESS + "/item/post", item, ITEM_RESPONSE_TYPE);
        if (!(response instanceof ItemResponse)) throw (AuctionException) response;
        return Utils.responseToItem((ItemResponse) response);
    }

    public static Bid addBid(Bid bid) throws AuctionException {
        Item item = bid.getItem();
        item.getCategory().setItems(new ArrayList<>());
        item.setBids(new ArrayList<>());
        Bid req = new Bid(bid.getPrice(), item, bid.getOwnerInfo());
        Object response = client.post(ADDRESS + "/bid/post", req, BID_RESPONSE_TYPE);
        if (!(response instanceof BidResponse)) throw (AuctionException) response;
        return Utils.responseToBid((BidResponse) response);
    }

    /*GET*/

    public static Category getCategory(int id) throws AuctionException {
        Object category = client.get(ADDRESS + String.format("/cat/%d", id), CATEGORY_RESPONSE_TYPE);
        if (!(category instanceof CategoryResponse)) throw (AuctionException) category;
        CategoryResponse categoryResponse = (CategoryResponse) category;
        return Utils.responseToCategory(categoryResponse);
    }

    public static Item getItem(int id) throws AuctionException {
        Object item = client.get(ADDRESS + String.format("/item/%d", id), ITEM_RESPONSE_TYPE);
        if (!(item instanceof ItemResponse)) throw (AuctionException) item;
        ItemResponse itemResponse = (ItemResponse) item;
        return Utils.responseToItem(itemResponse);
    }

    public static Bid getBid(long id) throws AuctionException {
        Object bid = client.get(ADDRESS + String.format("/bid/%d", id), BID_RESPONSE_TYPE);
        if (!(bid instanceof BidResponse)) throw (AuctionException) bid;
        BidResponse bidResponse = (BidResponse) bid;
        return Utils.responseToBid(bidResponse);
    }

    public static List<Category> getAllCategories() throws AuctionException {
        Object allCategories = client.get(ADDRESS + "/cat/", CATEGORY_RESPONSE_LIST_TYPE);
        if (!(allCategories instanceof List<?>)) throw new AuctionException("Unexpected error :" +
                GSON.toJson(allCategories));
        List<CategoryResponse> responses = (List<CategoryResponse>) allCategories;
        List<Category> result = new ArrayList<>();
        for (CategoryResponse response : responses) {
            result.add(Utils.responseToCategory(response));
        }
        return result;
    }


    public static List<Item> getAllItems() throws AuctionException {
        Object allItems = client.get(ADDRESS + "/item/", ITEM_RESPONSE_LIST_TYPE);
        if (!(allItems instanceof List<?>)) throw new AuctionException("Unexpected error :" +
                GSON.toJson(allItems));
        List<ItemResponse> responses = (List<ItemResponse>) allItems;
        List<Item> result = new ArrayList<>();
        for (ItemResponse response : responses) {
            result.add(Utils.responseToItem(response));
        }
        return result;
    }

    /*UPDATE*/

    public static void changeCategoryTitle(int id, String title) throws AuctionException {
        Object resp = client.put(ADDRESS + String.format("/cat/%d", id),
                title, EMPTY_RESPONSE_TYPE);
        if (!(resp instanceof EmptyResponse)) throw (AuctionException) resp;
    }

    public static void changeItem(int id, Item item) throws AuctionException {
        Object changedItem = client.put(ADDRESS + String.format("/item/%d", id), item, EMPTY_RESPONSE_TYPE);
        if (!(changedItem instanceof EmptyResponse)) throw new AuctionException("something went wrong");
        item.setId(id);
    }

    public static void raiseBidPrice(long id, Integer amount) throws AuctionException {
        Object resp = client.put(ADDRESS + String.format("/bid/%d", id),
                amount, EMPTY_RESPONSE_TYPE);
        if (!(resp instanceof EmptyResponse)) throw (AuctionException) resp;
    }

    /*DELETE*/

    public static void deleteAllCategories() {
        client.delete(ADDRESS + "/cat/", EMPTY_RESPONSE_TYPE);
    }

    public static void deleteAllItems() {
        client.delete(ADDRESS + "/item/", EMPTY_RESPONSE_TYPE);
    }

    public static void deleteAllBids() {
        client.delete(ADDRESS + "/bid/", EMPTY_RESPONSE_TYPE);
    }

    public static void deleteCategory(int id) {
        client.delete(ADDRESS + String.format("/cat/%d", id), EMPTY_RESPONSE_TYPE);
    }

    public static void deleteItem(int id) {
        client.delete(ADDRESS + String.format("/item/%d", id), EMPTY_RESPONSE_TYPE);
    }

    public static void deleteBid(long id) {
        client.delete(ADDRESS + String.format("/bid/%d", id), EMPTY_RESPONSE_TYPE);
    }
}
