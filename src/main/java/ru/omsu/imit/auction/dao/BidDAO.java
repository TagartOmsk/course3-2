package ru.omsu.imit.auction.dao;

import ru.omsu.imit.auction.model.Bid;
import ru.omsu.imit.auction.model.Item;

import java.util.List;

public interface BidDAO {

    Bid insert(Bid bid);

    void raisePrice(long id, int amount);

    Bid getById(long id);

    void delete(long id);

    void deleteAll();
}
