package ru.omsu.imit.auction.dao;

import ru.omsu.imit.auction.model.Category;

import java.util.List;

public interface CategoryDAO {

    Category insert(Category category);

    Category getById(int id);

    List<Category> getAll();

    void changeTitle(int id, String title);

    void delete(int id);

    void deleteAll();
}
