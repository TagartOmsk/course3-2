package ru.omsu.imit.auction.dao;

import ru.omsu.imit.auction.model.Category;
import ru.omsu.imit.auction.model.Item;

import java.util.List;

public interface ItemDAO {

    Item insert(Item item);

    Item getById(int id);

    List<Item> getAll();

//    Item changeName(int id, String name);
//
//    Item changeDescription(int id, String description);
//
//    Item changeCategory(int id, int catId);

    void changeItem(int id, String name, String description, int startPrice, int categoryId);

    void delete(int id);

    void deleteAll();
}
