package ru.omsu.imit.auction.dto.response;

import java.util.List;

public class ItemResponse {
    private int id;
    private String name;
    private String description;
    private int startPrice;
    private CategoryResponse category;
    private List<BidResponse> bids;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(int startPrice) {
        this.startPrice = startPrice;
    }

    public CategoryResponse getCategory() {
        return category;
    }

    public void setCategory(CategoryResponse category) {
        this.category = category;
    }

    public ItemResponse() {
    }

    public ItemResponse(int id, String name, String description, int startPrice, CategoryResponse category, List<BidResponse> bids) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.startPrice = startPrice;
        this.category = category;
        this.bids = bids;
    }

    public ItemResponse(ItemResponse other) {
        this(other.getId(), other.getName(), other.getDescription(), other.getStartPrice(), other.getCategory(), other.getBids());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<BidResponse> getBids() {
        return bids;
    }

    public void setBids(List<BidResponse> bids) {
        this.bids = bids;
    }
}
