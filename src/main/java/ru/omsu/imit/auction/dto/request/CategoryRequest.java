package ru.omsu.imit.auction.dto.request;

import java.util.List;

public class CategoryRequest implements AddRequest {
    private int id;
    private String title;
    private List<ItemRequest> items;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ItemRequest> getItems() {
        return items;
    }

    public void setItems(List<ItemRequest> items) {
        this.items = items;
    }

    public CategoryRequest() {
    }

    public CategoryRequest(int id, String title, List<ItemRequest> items) {
        this.id = id;
        this.title = title;
        this.items = items;
    }
}
