package ru.omsu.imit.auction.dto.request;

import java.util.List;

public class ItemRequest implements AddRequest{
    private int id;
    private String name;
    private String description;
    private int startPrice;
    private CategoryRequest category;
    private List<BidRequest> bids;

    public List<BidRequest> getBids() {
        return bids;
    }

    public void setBids(List<BidRequest> bids) {
        this.bids = bids;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(int startPrice) {
        this.startPrice = startPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CategoryRequest getCategory() {
        return category;
    }

    public void setCategory(CategoryRequest category) {
        this.category = category;
    }

    public ItemRequest(String name, String description, int startPrice, CategoryRequest category, List<BidRequest> bids) {
        this.name = name;
        this.description = description;
        this.startPrice = startPrice;
        this.category = category;
        this.bids = bids;
    }

    public ItemRequest() {
    }
}
