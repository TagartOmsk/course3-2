package ru.omsu.imit.auction.dto.response;


import java.util.List;

public class CategoryResponse {
    private int id;
    private String title;
    private List<ItemResponse> items;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ItemResponse> getItems() {
        return items;
    }

    public void setItems(List<ItemResponse> items) {
        this.items = items;
    }

    public CategoryResponse() {
    }

    public CategoryResponse(int id, String title, List<ItemResponse> items) {
        this.id = id;
        this.title = title;
        this.items = items;
    }

    public CategoryResponse(CategoryResponse other) {
        this(other.getId(), other.getTitle(), other.getItems());
    }
}
