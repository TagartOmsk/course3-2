package ru.omsu.imit.auction.dto.response;

public class BidResponse {
    private long id;
    private int price;
    private ItemResponse item;
    private String ownerInfo;


    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public ItemResponse getItem() {
        return item;
    }

    public void setItem(ItemResponse item) {
        this.item = item;
    }

    public String getOwnerInfo() {
        return ownerInfo;
    }

    public void setOwnerInfo(String ownerInfo) {
        this.ownerInfo = ownerInfo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BidResponse(long id, int price, ItemResponse item, String ownerInfo) {
        this.id = id;
        this.price = price;
        this.item = item;
        this.ownerInfo = ownerInfo;
    }

    public BidResponse(BidResponse other) {
        this(other.getId(), other.getPrice(), other.getItem(), other.getOwnerInfo());
    }
}
