package ru.omsu.imit.auction.dto.request;

public class BidRequest implements AddRequest {
    private long id;
    private int price;
    private ItemRequest item;
    private String ownerInfo;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BidRequest() {
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public ItemRequest getItem() {
        return item;
    }

    public void setItem(ItemRequest item) {
        this.item = item;
    }

    public String getOwnerInfo() {
        return ownerInfo;
    }

    public void setOwnerInfo(String ownerInfo) {
        this.ownerInfo = ownerInfo;
    }

    public BidRequest(int price, ItemRequest item, String ownerInfo) {
        this.price = price;
        this.item = item;
        this.ownerInfo = ownerInfo;
    }
}
