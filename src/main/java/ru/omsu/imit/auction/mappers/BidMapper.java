package ru.omsu.imit.auction.mappers;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import ru.omsu.imit.auction.model.Bid;
import ru.omsu.imit.auction.model.Item;

import java.util.List;

public interface BidMapper {

    @Insert("insert into bids values(#{bid.id}, #{bid.item.id}, #{bid.price}, #{bid.ownerInfo});")
    @Options(useGeneratedKeys = true, keyProperty = "bid.id", keyColumn = "id")
    public Integer insert(@Param("bid")Bid bid);

    @Update("update bids set price = #{price} where id = #{id}")
    public Integer raisePrice(@Param("id") long id, @Param("price") int amount);

    @Select("select id, price, item, owner_info as ownerInfo from bids where id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "item", column = "item", javaType = Item.class,
                    one = @One(select = "ru.omsu.imit.auction.mappers.ItemMapper.getById", fetchType = FetchType.LAZY))
    })
    public Bid getById(long id);

    @Select("select id, price, item, owner_info as ownerInfo from bids where item = #{item}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "item", column = "item", javaType = Item.class,
                    one = @One(select = "ru.omsu.imit.auction.mappers.ItemMapper.getById", fetchType = FetchType.LAZY))
    })
    public List<Bid> getByItem(int item);

    @Delete("delete from bids where id = #{id}")
    public void delete(long id);

    @Delete("delete from bids")
    void deleteAll();
}
