package ru.omsu.imit.auction.mappers;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import ru.omsu.imit.auction.model.Category;

import java.util.List;

public interface CategoryMapper {

    @Insert("insert into categories values(null, #{category.title})")
    @Options(useGeneratedKeys = true, keyProperty = "category.id", keyColumn = "id")
    Integer insert(@Param(value = "category") Category category);

    @Select("select id, title from categories where id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "items", column = "id", javaType = List.class,
                    many = @Many(select = "ru.omsu.imit.auction.mappers.ItemMapper.getByCategory", fetchType = FetchType.LAZY))
    })
    Category getById(int id);

    @Select("select id, title from categories")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "items", column = "id", javaType = List.class,
                    many = @Many(select = "ru.omsu.imit.auction.mappers.ItemMapper.getByCategory", fetchType = FetchType.LAZY))
    })
    List<Category> getAll();

    @Update("update categories set title = #{title} where id = #{id}")
    Integer changeTitle(@Param("id") int id, @Param("title") String title);

    @Delete("delete from categories where id = #{id}")
    void delete(int id);

    @Delete("delete from categories")
    void deleteAll();
}

