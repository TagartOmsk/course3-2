package ru.omsu.imit.auction.mappers;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import ru.omsu.imit.auction.model.Bid;
import ru.omsu.imit.auction.model.Category;
import ru.omsu.imit.auction.model.Item;

import java.util.List;

public interface ItemMapper {

    @Insert("insert into items values(#{item.id}, #{item.name}, #{item.description}, #{item.startPrice}, #{item.category.id})")
    @Options(useGeneratedKeys = true, keyProperty = "item.id", keyColumn = "id")
    Integer insert(@Param("item") Item item);

    @Select("select id, `name` as name, description, start_price as startPrice, category from items where id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "category", column = "category", javaType = Category.class,
                    one = @One(select = "ru.omsu.imit.auction.mappers.CategoryMapper.getById", fetchType = FetchType.LAZY)),
            @Result(property = "bids", column = "id", javaType = List.class,
                    many = @Many(select = "ru.omsu.imit.auction.mappers.BidMapper.getByItem", fetchType = FetchType.LAZY))
    })
    Item getById(int id);

    @Select("select id, `name` as name, description, start_price as startPrice, category from items")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "category", column = "category", javaType = Category.class,
                    one = @One(select = "ru.omsu.imit.auction.mappers.CategoryMapper.getById", fetchType = FetchType.LAZY)),
            @Result(property = "bids", column = "id", javaType = List.class,
                    many = @Many(select = "ru.omsu.imit.auction.mappers.BidMapper.getByItem", fetchType = FetchType.LAZY))
    })
    List<Item> getAll();

    @Select("select id, `name` as name, description, start_price as startPrice, category from items where category " +
            "= #{category}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "category", column = "category", javaType = Category.class,
                    one = @One(select = "ru.omsu.imit.auction.mappers.CategoryMapper.getById", fetchType = FetchType.LAZY)),
            @Result(property = "bids", column = "id", javaType = List.class,
                    many = @Many(select = "ru.omsu.imit.auction.mappers.BidMapper.getByItem", fetchType = FetchType.LAZY))
    })
    List<Item> getByCategory(int category);

    @Update("update items set name = #{name} where id = #{id}")
    Integer changeName(@Param("id") int id,@Param("name") String name);

    @Update("update items set description = #{description} where id = #{id}")
    Integer changeDescription(@Param("id") int id,@Param("description") String description);

    @Update("update items set category = #{category} where id = #{id}")
    Integer changeCategory(@Param("id") int id, @Param("category") int category);

    @Update("update items set start_price = #{startPrice} where id = #{id}")
    Integer changeStartPrice(@Param("id") int id, @Param("startPrice") int startPrice);

    @Delete("delete from items where id = #{id}")
    void delete(int id);

    @Delete("delete from items")
    void deleteAll();
}
