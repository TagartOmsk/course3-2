package ru.omsu.imit.auction.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Item {
    private int id;
    private String name;
    private String description;
    private int startPrice;
    private Category category;
    private List<Bid> bids;

    public Item() {}

    public Item(String name, String description, int startPrice, Category category, List<Bid> bids) {
        this(0, name, description, startPrice, category, bids);
    }
//
//    public Item(int id, String name, String description, int startPrice, Category category, Bid... bids) {
//        this(id, name, description, startPrice, category, Arrays.asList(bids));
//    }

    public Item(int id, String name, String description, int startPrice, Category category, List<Bid> bids) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.startPrice = startPrice;
        this.category = category;
        this.bids = bids;
    }

    public Item(int id, String name, String description, int startPrice, Category category) {
        this(id, name, description, startPrice, category, new ArrayList<>());
    }

    public Item(String name, String description, int startPrice, Category category) {
        this(0, name, description, startPrice, category, new ArrayList<>());
    }

    public void addBid(Bid bid) {
        bids.add(bid);
        bid.setItem(this);
    }

    public List<Bid> getBids() {
        return bids;
    }

    public void setBids(List<Bid> bids) {
        this.bids = bids;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(int startPrice) {
        this.startPrice = startPrice;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return getId() == item.getId() &&
                getStartPrice() == item.getStartPrice() &&
                Objects.equals(getName(), item.getName()) &&
                Objects.equals(getDescription(), item.getDescription()) &&
                Objects.equals(getCategory(), item.getCategory());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getDescription(), getStartPrice(), getCategory());
    }
}
