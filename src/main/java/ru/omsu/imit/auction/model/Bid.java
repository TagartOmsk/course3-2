package ru.omsu.imit.auction.model;

import java.util.Objects;

public class Bid {
    private long id;
    private int price;
    private Item item;
    private String ownerInfo;

    public Bid() {
    }

    public String getOwnerInfo() {
        return ownerInfo;
    }

    public void setOwnerInfo(String ownerInfo) {
        this.ownerInfo = ownerInfo;
    }

    public Bid(long id, int price, Item item, String ownerInfo) {
        this.id = id;
        this.price = price;
        this.item = item;
        this.ownerInfo = ownerInfo;
    }

    public Bid(int price, Item item, String ownerInfo) {
        this.price = price;
        this.item = item;
        this.ownerInfo = ownerInfo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bid bid = (Bid) o;
        return getId() == bid.getId() &&
                getPrice() == bid.getPrice() &&
                getItem().equals(bid.getItem()) &&
                Objects.equals(getOwnerInfo(), bid.getOwnerInfo());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getPrice(), getItem(), getOwnerInfo());
    }
}
