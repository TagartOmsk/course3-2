package ru.omsu.imit.auction.model;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Category {
    private int id;
    private String title;
    private List<Item> items;

    public Category(int id, String title) {
        this.id = id;
        this.title = title;
        items = new ArrayList<>();
    }

    public Category(int id, String title, Item...items) {
        this.id = id;
        this.title = title;
        this.items = Arrays.asList(items);
    }

    public Category(int id, String title, List<Item> items) {
        this.id = id;
        this.title = title;
        this.items = items;
    }

    public List<Item> getItems() {
        return items;
    }

    public void addItem(Item item) {
        items.add(item);
        item.setCategory(this);
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Category(String title) {
        this(0, title);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return getId() == category.getId() &&
                Objects.equals(getTitle(), category.getTitle());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitle());
    }
}
