package ru.omsu.imit.auction.services;

import ru.omsu.imit.auction.dto.request.BidRequest;
import ru.omsu.imit.auction.dto.request.CategoryRequest;
import ru.omsu.imit.auction.dto.request.ItemRequest;
import ru.omsu.imit.auction.exceptions.AuctionException;
import ru.omsu.imit.auction.model.Bid;
import ru.omsu.imit.auction.model.Category;
import ru.omsu.imit.auction.model.Item;

public class ValidationService {
    public static Bid isBidValid(BidRequest bid) throws AuctionException {
        if (bid == null || bid.getItem() == null || bid.getPrice() <= bid.getItem().getStartPrice())
            throw new AuctionException("Invalid Bid");
        return new Bid(bid.getId(), bid.getPrice(), isItemValid(bid.getItem()), bid.getOwnerInfo());
    }

    public static Item isItemValid(ItemRequest item) throws AuctionException {
        if (item == null || item.getCategory() == null || item.getStartPrice() < 0 ||
                item.getName() == null) throw new AuctionException("Invalid Item");
        return new Item(item.getId(), item.getName(), item.getDescription(),
                item.getStartPrice(), isCategoryValid(item.getCategory()));
    }

    public static Category isCategoryValid(CategoryRequest cat) throws AuctionException {
        if (cat == null) throw new AuctionException("Invalid Category");
        if (cat.getTitle() == null) throw new AuctionException("Invalid Category");
        return new Category(cat.getId(), cat.getTitle());
    }
}
