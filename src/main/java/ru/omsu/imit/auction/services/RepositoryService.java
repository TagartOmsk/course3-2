package ru.omsu.imit.auction.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.auction.dao.BidDAO;
import ru.omsu.imit.auction.dao.CategoryDAO;
import ru.omsu.imit.auction.dao.ItemDAO;
import ru.omsu.imit.auction.daoimpl.BidDAOImpl;
import ru.omsu.imit.auction.daoimpl.CategoryDAOImpl;
import ru.omsu.imit.auction.daoimpl.ItemDAOImpl;
import ru.omsu.imit.auction.dto.request.BidRequest;
import ru.omsu.imit.auction.dto.request.CategoryRequest;
import ru.omsu.imit.auction.dto.request.ItemRequest;
import ru.omsu.imit.auction.dto.response.CategoryResponse;
import ru.omsu.imit.auction.dto.response.EmptyResponse;
import ru.omsu.imit.auction.dto.response.ItemResponse;
import ru.omsu.imit.auction.exceptions.AuctionException;
import ru.omsu.imit.auction.model.Bid;
import ru.omsu.imit.auction.model.Category;
import ru.omsu.imit.auction.model.Item;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class RepositoryService {
    private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryService.class);
    private static final Gson GSON = new GsonBuilder().create();
    private BidDAO bidDAO = new BidDAOImpl();
    private ItemDAO itemDAO = new ItemDAOImpl();
    private CategoryDAO categoryDAO = new CategoryDAOImpl();

    /*BIDS*/

    public Response insertBid(String json) {
        LOGGER.debug("insert bid");
        try {
            BidRequest request = GSON.fromJson(json, BidRequest.class);
            Bid bid = ValidationService.isBidValid(request);
            Bid addedBid = bidDAO.insert(bid);
            String response = GSON.toJson(Utils.createResponse(addedBid));
            return Response.ok(response, MediaType.APPLICATION_JSON_TYPE).build();
        } catch (AuctionException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(GSON.toJson(ex)).build();
        }
    }

    public Response raiseBidPrice(long id, String json) {
        int amount = GSON.fromJson(json, Integer.class);
        LOGGER.debug(String.format("raising price for bid %d by %d", id, amount));
        if (amount <= 0) return Response.status(Response.Status.BAD_REQUEST).entity(GSON.toJson(
                new AuctionException("Invalid price")
        )).build();
        bidDAO.raisePrice(id, amount);
        String response = GSON.toJson(new EmptyResponse());
        return Response.ok(response, MediaType.APPLICATION_JSON_TYPE).build();
    }

    public Response getBid(long id) {
        LOGGER.debug("get bid by id " + id);
        try {
            Bid bid = bidDAO.getById(id);
            String response = GSON.toJson(Utils.createResponse(bid));
            return Response.ok(response, MediaType.APPLICATION_JSON_TYPE).build();
        } catch (NullPointerException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(GSON.toJson(
                    new AuctionException("No bid with such id")
            )).build();
        }
    }

    public Response deleteBid(long id) {
        LOGGER.debug("delete bid " + id);
        bidDAO.delete(id);
        String response = GSON.toJson(new EmptyResponse());
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

    public Response deleteAll() {
        LOGGER.debug("delete all bids");
        bidDAO.deleteAll();
        String response = GSON.toJson(new EmptyResponse());
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

    /*ITEMS*/

    public Response insertItem(String json) {
        LOGGER.debug("insert item");
        try {
            ItemRequest request = GSON.fromJson(json, ItemRequest.class);
            Item item = ValidationService.isItemValid(request);
            Item addedItem = itemDAO.insert(item);
            ItemResponse resp = Utils.createResponse(addedItem);
            String response = GSON.toJson(resp);
            return Response.ok(response, MediaType.APPLICATION_JSON_TYPE).build();
        } catch (AuctionException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(GSON.toJson(ex)).build();
        }
    }

    public Response getItemById(int id) {
        LOGGER.debug("get item by id " + id);
        try {
            Item item = itemDAO.getById(id);
            String response = GSON.toJson(Utils.createResponse(item));
            return Response.ok(response, MediaType.APPLICATION_JSON_TYPE).build();
        } catch (NullPointerException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(GSON.toJson(
                    new AuctionException("No item with such id", ex)
            )).build();
        }
    }

    public Response getAllItems() {
        LOGGER.debug("get all items");
        List<Item> items = itemDAO.getAll();
        List<ItemResponse> responseList = new ArrayList<>(items.size());
        for (Item item : items) {
            responseList.add(Utils.createResponse(item));
        }
        String response = GSON.toJson(responseList);
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

    public Response changeItem(int id, String json) {
        LOGGER.debug("change item " + id);
        Item oldItem = itemDAO.getById(id);
        ItemRequest request = GSON.fromJson(json, ItemRequest.class);
        if (request.getName() == null || request.getName().equals("")) {
            request.setName(oldItem.getName());
        }
        if (request.getDescription() == null || request.getDescription().equals("")) {
            request.setDescription(oldItem.getDescription());
        }
        int categoryId = request.getCategory().getId();
        if (categoryDAO.getById(categoryId) == null) {
            categoryId = oldItem.getCategory().getId();
        }
        if (request.getStartPrice() <= 0) {
            request.setStartPrice(oldItem.getStartPrice());
        }
        itemDAO.changeItem(id, request.getName(), request.getDescription(),
                request.getStartPrice(), categoryId);

        String response = GSON.toJson(new EmptyResponse());
        return Response.ok(response, MediaType.APPLICATION_JSON_TYPE).build();
    }

    public Response deleteItem(int id) {
        LOGGER.debug("delete item " + id);
        itemDAO.delete(id);
        String response = GSON.toJson(new EmptyResponse());
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

    public Response deleteAllItems() {
        LOGGER.debug("delete all items");
        itemDAO.deleteAll();
        String response = GSON.toJson(new EmptyResponse());
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

    /*CATEGORIES*/

    public Response insertCategory(String json) {
        LOGGER.debug("insert category");
        try {
            CategoryRequest request = GSON.fromJson(json, CategoryRequest.class);
            Category cat = ValidationService.isCategoryValid(request);
            Category addedCat = categoryDAO.insert(cat);
            String response = GSON.toJson(Utils.createResponse(addedCat));
            return Response.ok(response, MediaType.APPLICATION_JSON_TYPE).build();
        } catch (AuctionException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(GSON.toJson(ex)).build();
        }
    }

    public Response getCategoryById(int id) {
        LOGGER.debug("get category by id" + id);
        try {
            Category category = categoryDAO.getById(id);
            String response = GSON.toJson(Utils.createResponse(category));
            return Response.ok(response, MediaType.APPLICATION_JSON_TYPE).build();
        } catch (NullPointerException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(GSON.toJson(
                    new AuctionException("No item with such id", ex)
            )).build();
        }
    }

    public Response getAllCategories() {
        LOGGER.debug("get all categories");
        List<Category> categories = categoryDAO.getAll();
        List<CategoryResponse> responseList = new ArrayList<>(categories.size());
        for (Category category : categories) {
            responseList.add(Utils.createResponse(category));
        }
        String response = GSON.toJson(responseList);
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

    public Response changeCategoryTitle(int id, String title) {
        LOGGER.debug("change category title");
        if (title == null || title.equals("")) return Response.status(Response.Status.BAD_REQUEST).entity(
                GSON.toJson(new AuctionException("Invalid title"))
        ).build();
        categoryDAO.changeTitle(id, title);
        String response = GSON.toJson(new EmptyResponse());
        return Response.ok(response, MediaType.APPLICATION_JSON_TYPE).build();
    }

    public Response deleteCategory(int id) {
        LOGGER.debug("delete category " + id);
        categoryDAO.delete(id);
        String response = GSON.toJson(new EmptyResponse());
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

    public Response deleteAllCategories() {
        LOGGER.debug("delete all categories");
        categoryDAO.deleteAll();
        String response = GSON.toJson(new EmptyResponse());
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

}
