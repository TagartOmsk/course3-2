package ru.omsu.imit.auction.services;

import ru.omsu.imit.auction.dto.response.BidResponse;
import ru.omsu.imit.auction.dto.response.CategoryResponse;
import ru.omsu.imit.auction.dto.response.ItemResponse;
import ru.omsu.imit.auction.model.Bid;
import ru.omsu.imit.auction.model.Category;
import ru.omsu.imit.auction.model.Item;

import java.util.ArrayList;
import java.util.List;

public class Utils {
    public static BidResponse createResponse(Bid bid) {
        bid.getItem().setBids(new ArrayList<>());
        ItemResponse itemResponse = createResponse(bid.getItem());
        return new BidResponse(bid.getId(), bid.getPrice(), itemResponse, bid.getOwnerInfo());
    }


    public static ItemResponse createResponse(Item item) {
        item.getCategory().setItems(new ArrayList<>());
        CategoryResponse categoryResponse = createResponse(item.getCategory());
        ItemResponse result = new ItemResponse(item.getId(), item.getName(), item.getDescription(), item.getStartPrice(),
                categoryResponse, null);

        List<BidResponse> bidResponses = new ArrayList<>();
        for (Bid bid : item.getBids()) {
            bidResponses.add(new BidResponse(bid.getId(), bid.getPrice(), null, bid.getOwnerInfo()));
        }
        result.setBids(bidResponses);
//        ItemResponse shadow = new ItemResponse(result);
//        shadow.setCategory(null);
//        categoryResponse.getItems().add(shadow);
        return result;
    }

    public static CategoryResponse createResponse(Category category) {
        CategoryResponse result = new CategoryResponse(category.getId(), category.getTitle(), null);
        List<ItemResponse> itemResponses = new ArrayList<>();
        for (Item item : category.getItems()) {
            ItemResponse newItemResponse = new ItemResponse(item.getId(), item.getName(), item.getDescription(), item.getStartPrice(),
                    null, null);
            List<BidResponse> bidResponses = new ArrayList<>();
            for (Bid bid : item.getBids()) {
                bidResponses.add(new BidResponse(bid.getId(), bid.getPrice(), null, bid.getOwnerInfo()));
            }
            newItemResponse.setBids(bidResponses);
            itemResponses.add(newItemResponse);
        }
        result.setItems(itemResponses);
        return result;
    }

    public static Bid responseToBid(BidResponse response) {
        Bid result = new Bid(response.getId(), response.getPrice(), responseToItem(response.getItem()), response.getOwnerInfo());
        result.getItem().getBids().remove(result);
        result.getItem().addBid(result);
        return result;
    }

    private static Bid responseToBidLazy(BidResponse response, Item item) {
        return new Bid(response.getId(), response.getPrice(), item, response.getOwnerInfo());
    }

    public static Item responseToItem(ItemResponse response) {
        Item result = new Item(response.getId(), response.getName(), response.getDescription(), response.getStartPrice(),
                responseToCategory(response.getCategory()));
        Category category = result.getCategory();
        category.getItems().remove(result);
        category.addItem(result);
        for (BidResponse bidResponse : response.getBids()) {
            result.addBid(responseToBidLazy(bidResponse, result));
        }
        return result;
    }

    private static Item responseToItemLazy(ItemResponse response, Category category) {
        Item result = new Item(response.getId(), response.getName(), response.getDescription(), response.getStartPrice(),
                category);
        for (BidResponse bidResponse : response.getBids()) {
            result.addBid(responseToBidLazy(bidResponse, result));
        }
        return result;
    }

    public static Category responseToCategory(CategoryResponse response) {
        Category result = new Category(response.getId(), response.getTitle());
        for (ItemResponse itemResponse : response.getItems()) {
            result.addItem(responseToItemLazy(itemResponse, result));
        }
        return result;
    }

//    public static void bind(Category category) {
//        for (Item item : category.getItems()) {
//            item.setCategory(category);
//            for (Bid bid : item.getBids()) {
//                bid.setItem(item);
//            }
//        }
//    }
}
