package ru.omsu.imit.auction.config;

import org.glassfish.jersey.server.ResourceConfig;

public class AuctionResourceConfig extends ResourceConfig {
    public AuctionResourceConfig(){
        packages("ru.omsu.imit.auction.controller");
    }
}
