package ru.omsu.imit.auction.controller;

import ru.omsu.imit.auction.services.RepositoryService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/auction/item")
public class ItemController {

    private RepositoryService service = new RepositoryService();

    @POST
    @Path("/post")
    @Consumes("application/json")
    @Produces("application/json")
    public Response postItem(String json) {
        return service.insertItem(json);
    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getItemById(@PathParam("id") int id) {
        return service.getItemById(id);
    }

    @GET
    @Path("/")
    @Produces("application/json")
    public Response getAllItems() {
        return service.getAllItems();
    }

    @PUT
    @Path("/{id}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response changeItem(@PathParam("id") int id, String json) {
        return service.changeItem(id, json);
    }


    @DELETE
    @Path("/{id}")
    @Produces("application/json")
    public Response deleteItem(@PathParam("id") int id) {
        return service.deleteItem(id);
    }

    @DELETE
    @Path("/")
    @Produces("application/json")
    public Response deleteAllItems() {
        return service.deleteAllItems();
    }
}
