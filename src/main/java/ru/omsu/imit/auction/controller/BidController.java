package ru.omsu.imit.auction.controller;

import ru.omsu.imit.auction.services.RepositoryService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;


@Path("/auction/bid")
public class BidController {

    private RepositoryService service = new RepositoryService();

    @POST
    @Path("/post")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addBid(String json) {
        return service.insertBid(json);
    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getById(@PathParam(value = "id") long id) {
        return service.getBid(id);
    }

    @PUT
    @Path("/{id}")
    @Produces("application/json")
    @Consumes("application/json")
    public Response raisePrice(@PathParam(value = "id") long id, String json) {
        return service.raiseBidPrice(id, json);
    }

    @DELETE
    @Path("/{id}")
    @Produces("application/json")
    public Response deleteById(@PathParam(value = "id") long id) {
        return service.deleteBid(id);
    }

    @DELETE
    @Path("/")
    @Produces("application/json")
    public Response deleteAll(){
        return Response.ok().build();
    }
}
