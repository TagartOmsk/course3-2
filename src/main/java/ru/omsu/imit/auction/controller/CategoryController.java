package ru.omsu.imit.auction.controller;

import ru.omsu.imit.auction.services.RepositoryService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/auction/cat")
public class CategoryController {

    private RepositoryService service = new RepositoryService();

    @POST
    @Path("/post")
    @Consumes("application/json")
    @Produces("application/json")
    public Response postCategory(String json){
        return service.insertCategory(json);
    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getCategory(@PathParam("id")int id){
        return service.getCategoryById(id);
    }

    @GET
    @Path("/")
    @Produces("application/json")
    public Response getAllCategories(){
        return service.getAllCategories();
    }

    @PUT
    @Path("/{id}")
    @Produces("application/json")
    @Consumes("application/json")
    public Response changeCategoryTitle(@PathParam("id") int id, String title){
        return service.changeCategoryTitle(id, title);
    }

    @DELETE
    @Path("/{id}")
    @Produces("application/json")
    public Response deleteCategory(@PathParam("id")int id){
        return service.deleteCategory(id);
    }

    @DELETE
    @Path("/")
    @Produces("application/json")
    public Response deleteAllCategories(){
        return service.deleteAllCategories();
    }
}
