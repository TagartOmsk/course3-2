package ru.omsu.imit.auction.daoimpl;

import org.apache.ibatis.session.SqlSession;
import ru.omsu.imit.auction.mappers.BidMapper;
import ru.omsu.imit.auction.mappers.CategoryMapper;
import ru.omsu.imit.auction.mappers.ItemMapper;
import ru.omsu.imit.auction.utils.MyBatisUtils;

public class BaseDAOImpl {

    protected SqlSession getSession() {
        return MyBatisUtils.getSqlSessionFactory().openSession();
    }

    protected BidMapper getBidMapper (SqlSession sqlSession) {
        return sqlSession.getMapper(BidMapper.class);
    }

    protected ItemMapper getItemMapper (SqlSession sqlSession) {
        return sqlSession.getMapper(ItemMapper.class);
    }

    protected CategoryMapper getCategoryMapper (SqlSession sqlSession) {
        return sqlSession.getMapper(CategoryMapper.class);
    }
}
