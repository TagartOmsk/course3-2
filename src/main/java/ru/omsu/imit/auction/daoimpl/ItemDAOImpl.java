package ru.omsu.imit.auction.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.auction.dao.ItemDAO;
import ru.omsu.imit.auction.mappers.ItemMapper;
import ru.omsu.imit.auction.model.Category;
import ru.omsu.imit.auction.model.Item;

import java.util.List;

public class ItemDAOImpl extends BaseDAOImpl implements ItemDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemDAOImpl.class);

    @Override
    public Item insert(Item item) {
        LOGGER.debug("DAO Insert Item {}", item);
        try (SqlSession sqlSession = getSession()) {
            try {
                getItemMapper(sqlSession).insert(item);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't insert Item {} {}", item, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return item;
    }

    @Override
    public Item getById(int id){
        try (SqlSession sqlSession = getSession()) {
            return getItemMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get Item by id {} {}", id, ex);
            throw ex;
        }
    }

    @Override
    public List<Item> getAll() {
        try (SqlSession sqlSession = getSession()) {
            return getItemMapper(sqlSession).getAll();
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get all Items", ex);
            throw ex;
        }
    }

    @Override
    public void changeItem(int id, String name, String description, int startPrice, int categoryId) {
        try (SqlSession sqlSession = getSession()) {
            try {
                ItemMapper mapper = getItemMapper(sqlSession);
                mapper.changeName(id, name);
                mapper.changeDescription(id, description);
                mapper.changeStartPrice(id, startPrice);
                mapper.changeCategory(id, categoryId);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't Change Item {}", id, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    //    @Override
//    public Item changeName(int id, String name) {
//        Item item;
//        try (SqlSession sqlSession = getSession()) {
//            try {
//                item = getItemMapper(sqlSession).changeName(id, name);
//            } catch (RuntimeException ex) {
//                LOGGER.debug("Can't Change Name {} {} {}", id, name, ex);
//                sqlSession.rollback();
//                throw ex;
//            }
//            sqlSession.commit();
//        }
//        return item;
//    }
//
//    @Override
//    public Item changeDescription(int id, String description) {
//        Item item;
//        try (SqlSession sqlSession = getSession()) {
//            try {
//                item = getItemMapper(sqlSession).changeDescription(id, description);
//            } catch (RuntimeException ex) {
//                LOGGER.debug("Can't Change Description {} {} {}", id, description, ex);
//                sqlSession.rollback();
//                throw ex;
//            }
//            sqlSession.commit();
//        }
//        return item;
//    }
//
//    @Override
//    public Item changeCategory(int id, int catId) {
//        Item item;
//        try (SqlSession sqlSession = getSession()) {
//            try {
//                item = getItemMapper(sqlSession).changeCategory(id, catId);
//            } catch (RuntimeException ex) {
//                LOGGER.debug("Can't Change Name {} {} {}", id, catId, ex);
//                sqlSession.rollback();
//                throw ex;
//            }
//            sqlSession.commit();
//        }
//        return item;
//    }

    @Override
    public void delete(int id) {
        try (SqlSession sqlSession = getSession()) {
            try {
                getItemMapper(sqlSession).delete(id);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't delete Item with id {} {}", id, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("DAO Delete All Bids");
        try (SqlSession sqlSession = getSession()) {
            try {
                getItemMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't delete all Items ", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }
}
