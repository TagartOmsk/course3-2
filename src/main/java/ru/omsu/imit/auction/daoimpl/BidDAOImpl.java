package ru.omsu.imit.auction.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.auction.dao.BidDAO;
import ru.omsu.imit.auction.mappers.BidMapper;
import ru.omsu.imit.auction.model.Bid;
import ru.omsu.imit.auction.model.Item;

import java.util.List;

public class BidDAOImpl extends BaseDAOImpl implements BidDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(BidDAOImpl.class);

    @Override
    public Bid insert(Bid bid) {
        LOGGER.debug("DAO Insert Bid {}", bid);
        try (SqlSession sqlSession = getSession()) {
            try {
                getBidMapper(sqlSession).insert(bid);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't insert Bid {} {}", bid, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return bid;
    }

    @Override
    public void raisePrice(long id, int amount) {
        try (SqlSession sqlSession = getSession()) {
            try {
                getBidMapper(sqlSession).raisePrice(id, amount);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't Change Price {} {} {}", id, amount, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public Bid getById(long id) {
        try (SqlSession sqlSession = getSession()) {
            return getBidMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get Bid by id {} {}", id, ex);
            throw ex;
        }
    }

    @Override
    public void delete(long id) {
        try (SqlSession sqlSession = getSession()) {
            try {
                getBidMapper(sqlSession).delete(id);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't delete Bid with id {} {}", id, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("DAO Delete All Bids");
        try (SqlSession sqlSession = getSession()) {
            try {
                getBidMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't delete all Bids {}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }
}
