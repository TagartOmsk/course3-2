package ru.omsu.imit.auction.daoimpl;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.auction.dao.CategoryDAO;
import ru.omsu.imit.auction.model.Category;

import java.util.List;

public class CategoryDAOImpl extends BaseDAOImpl implements CategoryDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(CategoryDAOImpl.class);

    @Override
    public Category insert(Category category) {
        LOGGER.debug("DAO Insert Category {}", category);
        try (SqlSession sqlSession = getSession()) {
            try {
                getCategoryMapper(sqlSession).insert(category);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't insert Category {} {}", category, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return category;
    }

    @Override
    public Category getById(int id) {
        try (SqlSession sqlSession = getSession()) {
            return getCategoryMapper(sqlSession).getById(id);
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get Category by id {} {}", id, ex);
            throw ex;
        }
    }

    @Override
    public List<Category> getAll() {
        try (SqlSession sqlSession = getSession()) {
            return getCategoryMapper(sqlSession).getAll();
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get all Categories", ex);
            throw ex;
        }
    }

    @Override
    public void changeTitle(int id, String title) {
        try (SqlSession sqlSession = getSession()) {
            try {
                getCategoryMapper(sqlSession).changeTitle(id, title);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't Change Title {} {} {}", id, title, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void delete(int id) {
        try (SqlSession sqlSession = getSession()) {
            try {
                getCategoryMapper(sqlSession).delete(id);
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't delete Category with id {} {}", id, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("DAO Delete All Categories");
        try (SqlSession sqlSession = getSession()) {
            try {
                getCategoryMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't delete all Categories", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }
}
