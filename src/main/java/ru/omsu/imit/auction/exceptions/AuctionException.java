package ru.omsu.imit.auction.exceptions;

public class AuctionException extends Throwable {
    public AuctionException() {
    }

    public AuctionException(String message) {
        super(message);
    }

    public AuctionException(String message, Throwable cause) {
        super(message, cause);
    }
}
