package ru.omsu.imit.auction;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ru.omsu.imit.auction.exceptions.AuctionException;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.reflect.Type;

public class AuctionClient {

    private static final Gson GSON = new GsonBuilder().create();

    private static Client createClient() {
        return ClientBuilder.newClient();
    }

    public Object get(String url, Type type) {
        Client client = createClient();
        WebTarget myResource = client.target(url);
        Invocation.Builder builder = myResource.request(MediaType.APPLICATION_JSON);
        Response response = (Response) builder.get();
        String body = response.readEntity(String.class);
        int httpCode = response.getStatus();
        Object obj;
        if (httpCode == Response.Status.OK.getStatusCode()) {
            obj = GSON.fromJson(body, type);
        } else {
            obj = GSON.fromJson(body, AuctionException.class);
        }
        client.close();
        return obj;
    }

    public Object post(String url, Object object, Type type) {
        Client client = createClient();
        WebTarget myResource = client.target(url);
        Invocation.Builder builder = myResource.request(MediaType.APPLICATION_JSON);
        Response response = builder.post(Entity.json(object));
        String body = response.readEntity(String.class);
        int httpCode = response.getStatus();
        Object obj;
        if (httpCode == Response.Status.OK.getStatusCode()) {
            obj = GSON.fromJson(body, type);
        } else {
            obj = GSON.fromJson(body, AuctionException.class);
        }
        client.close();
        return obj;
    }

    public Object put(String url, Object object, Type type) {
        Client client = createClient();
        WebTarget myResource = client.target(url);
        Invocation.Builder builder = myResource.request(MediaType.APPLICATION_JSON);
        Response response = builder.put(Entity.json(object));
        String body = response.readEntity(String.class);
        int httpCode = response.getStatus();
        Object obj;
        if (httpCode == Response.Status.OK.getStatusCode()) {
            obj = GSON.fromJson(body, type);
        } else {
            obj = GSON.fromJson(body, AuctionException.class);
        }
        client.close();
        return obj;
    }

    public Object delete(String url, Type type) {
        Client client = createClient();
        WebTarget myResource = client.target(url);
        Invocation.Builder builder = myResource.request(MediaType.APPLICATION_JSON);
        Response response = (Response) builder.delete();
        String body = response.readEntity(String.class);
        int httpCode = response.getStatus();
        Object obj;
        if (httpCode == Response.Status.OK.getStatusCode()) {
            obj = GSON.fromJson(body, type);
        } else {
            obj = GSON.fromJson(body, AuctionException.class);
        }
        client.close();
        return obj;
    }

//    public static void main(String[] args) {
//        AuctionClient client = new AuctionClient();
//        Category homeCat = new Category(0, "Home"),
//                roboCat = new Category(0, "Robots");
//        client.delete("http://localhost:9000/auction/cat/", EmptyResponse.class);
//        client.delete("http://localhost:9000/auction/item/", EmptyResponse.class);
//        client.post("http://localhost:9000/auction/cat/post", homeCat);
//        client.post("http://localhost:9000/auction/cat/post", roboCat);
//
//        Object response = client.get("http://localhost:9000/auction/cat/", List.class);
//        if(response instanceof List<?>) {
//            List<CategoryResponse> responseList = (List<CategoryResponse>) response;
//            for(int i = 0; i < responseList.size(); i++) {
//                String str = GSON.toJson(responseList.get(i));
//                CategoryResponse resp = GSON.fromJson(str, CategoryResponse.class);
//                if(resp.getTitle().equalsIgnoreCase("robots")) roboCat.setId(resp.getId());
//                if(resp.getTitle().equalsIgnoreCase("home")) homeCat.setId(resp.getId());
//            }
//        } else {
//            return;
//        }
//
//        client.post("http://localhost:9000/auction/item/post", new Item(0, "Ancient Mirror", null,
//                5, homeCat), ItemResponse.class);
//        client.post("http://localhost:9000/auction/item/post", new Item(0, "Vacuum robot", null,
//                3, homeCat), ItemResponse.class);
//        ItemResponse resp = (ItemResponse) client.get("http://localhost:9000/auction/item/get/", ItemResponse.class);
//
//        Item changedCat = new Item(resp.getId(), resp.getName(), resp.getDescription(), resp.getStartPrice(),
//                roboCat);
//        client.put("http://localhost:9000/auction/item/put", changedCat, ItemResponse.class);
//    }
}
