package ru.omsu.imit.rest;

import com.google.gson.Gson;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class RestTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestTest.class);
    private static final int port = 9000;
    private static final Gson json = new Gson();

    protected static Client client = new Client(port, "localhost");
    private static String baseURL;

    private static void initialize() {
        String hostName = null;
        try {
            hostName = InetAddress.getLocalHost().getCanonicalHostName();
        } catch (UnknownHostException e) {
            LOGGER.debug("Can't determine my own host name", e);
        }
        baseURL = "http://" + hostName + ":" + port + "/api";
    }

    @BeforeClass
    public static void startServer() {
        initialize();
        DatabaseEmulator.createServer();
    }


    @AfterClass
    public static void stopServer() {
        DatabaseEmulator.stopServer();
    }

    @Test
    public void testAdd() {
        Person person = new Person("Master", "Grudge", 30);
        Assert.assertEquals("{}", json.toJson(client.post(baseURL+"/people", person, RequestState.class)));
    }

    @Test
    public void testGet() {
        Person person = new Person("Master", "Grudge", 30);
        Assert.assertEquals("{}", json.toJson(client.post(baseURL+"/people", person, RequestState.class)));
        Assert.assertEquals(person, json.fromJson((String)client.get(baseURL+"/people?age=30", String.class), Person.class));
    }
}
