package ru.omsu.imit.socketserver;

import com.google.gson.Gson;
import org.junit.*;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ServerTest {
    static Client client = new Client(9000, "localhost");
    static Gson json = new Gson();

    @Test
    public void testAdd() {
        Person person = new Person("Heavy", "Rain", 24);
        List<String> res = client.executeRequests("add " + json.toJson(person), "add {firstName: ыщ}");
        if (res == null) fail();
        assertEquals("{}", res.get(0));
        assertEquals(json.toJson(new Error("Invalid data")), res.get(1));
    }

    @Test
    public void testGet() {
        Person person = new Person("Check", "Man", 99);
        List<String> res = client.executeRequests("add " + json.toJson(person), "get " + json.toJson(person));
        if (res == null) fail();
        assertEquals(json.toJson(person), res.get(1));
    }

    @AfterClass
    public static void shut(){
        client.executeRequests("quit");
    }
}