package ru.omsu.imit.persons;

import com.google.gson.Gson;
import org.junit.Test;

public class ServerTest {

    @Test
    public void testJson() {
        Server serv = new Server();
        Client client = new Client(serv);
        Gson json = new Gson();
        client.get("{\"lastName\":\"Starving\"}");
        client.add("{\"lastName\":\"Starving\"}");
        client.add("{\"lastName\":\"Starving\", \"firstName\":\"Richard\",\"age\":34}");
        client.get("{\"lastName\":\"Starving\"}");
    }
}