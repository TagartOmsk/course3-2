package ru.omsu.imit.auction;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.omsu.imit.auction.exceptions.AuctionException;
import ru.omsu.imit.auction.model.Bid;
import ru.omsu.imit.auction.model.Category;
import ru.omsu.imit.auction.model.Item;

import java.util.List;

import static org.junit.Assert.*;

public class AuctionTest {
    private static Category baroque = new Category("Baroque");
    private static Category renaissance = new Category("Renaissance");
    private static Category modern = new Category("Modern");
    private static Category postmodern = new Category("Postmodern");

    @BeforeClass
    public static void init() {
        ServerStarter.attachShutDownHook();
        ServerStarter.createServer();
    }

    @Before
    public void clear() {
        UserFriendlyClient.deleteAllBids();
        UserFriendlyClient.deleteAllItems();
        UserFriendlyClient.deleteAllCategories();
    }

    @Test
    public void testAdd() {
        try {
            Category addedCat = UserFriendlyClient.addCategory(baroque);
            assertEquals(addedCat.getTitle(), baroque.getTitle());
            baroque = addedCat;
            Item vase = new Item("Baroque vase", "A regular baroque vase", 1000, baroque);
            Item addedItem = UserFriendlyClient.addItem(vase);
            vase.setId(addedItem.getId());
            assertEquals(vase, addedItem);
            Bid bid1 = new Bid(1001, vase, "2WDTRE");
            Bid addedBid = UserFriendlyClient.addBid(bid1);
            bid1.setId(addedBid.getId());
            assertEquals(bid1, addedBid);
            Bid bid2 = new Bid(1100, vase, "@cha55el");
            addedBid = UserFriendlyClient.addBid(bid2);
            bid2.setId(addedBid.getId());
            assertEquals(bid2, addedBid);
        } catch (AuctionException ex) {
            fail();
        }
    }

    @Test
    public void testGetAll() {
        try {
            baroque.setId(UserFriendlyClient.addCategory(baroque).getId());
            renaissance.setId(UserFriendlyClient.addCategory(renaissance).getId());
            modern.setId(UserFriendlyClient.addCategory(modern).getId());
            postmodern.setId(UserFriendlyClient.addCategory(postmodern).getId());
            Item vase = new Item("Baroque vase", "A regular baroque vase", 1000, baroque);
            Item addedVase = UserFriendlyClient.addItem(vase);
            Item teapot = new Item("Baroque teapot", "A regular baroque teapot", 500, baroque);
            Item addedTeapot = UserFriendlyClient.addItem(teapot);
            List<Category> allCats = UserFriendlyClient.getAllCategories();
            assertEquals(4, allCats.size());
            Category baroque1 = allCats.get(allCats.indexOf(baroque));
            assertEquals(2, baroque1.getItems().size());
            List<Item> allItems = UserFriendlyClient.getAllItems();
            assertEquals(2, allItems.size());
        } catch (AuctionException ex) {
            fail();
        }

    }

    @Test
    public void testDeleteCascade() {
        try {
            baroque.setId(UserFriendlyClient.addCategory(baroque).getId());
            Item vase = new Item("Baroque vase", "A regular baroque vase", 1000, baroque);
            Item addedVase = UserFriendlyClient.addItem(vase);
            Item teapot = new Item("Baroque teapot", "A regular baroque teapot", 500, baroque);
            Item addedTeapot = UserFriendlyClient.addItem(teapot);

            List<Category> allCats = UserFriendlyClient.getAllCategories();

            Bid bid1 = new Bid(1001, addedVase, "2WDTRE");
            Bid bid2 = new Bid(1100, addedVase, "@cha55el");

            Bid bid3 = new Bid(550, addedTeapot, "@cha55el");

            bid1.setId(UserFriendlyClient.addBid(bid1).getId());
            bid2.setId(UserFriendlyClient.addBid(bid2).getId());
            bid3.setId(UserFriendlyClient.addBid(bid3).getId());

            UserFriendlyClient.deleteItem(addedVase.getId());

            List<Item> items = UserFriendlyClient.getAllItems();
            assertEquals(addedTeapot, items.get(0));
            int bid_count = 0;
            for (Item item : items) {
                bid_count += item.getBids().size();
            }

            assertEquals(1, bid_count);
        } catch (AuctionException ex) {
            fail();
        }
    }

    @Test
    public void testChangeItem() {
        try {
            baroque.setId(UserFriendlyClient.addCategory(baroque).getId());
            modern.setId(UserFriendlyClient.addCategory(modern).getId());
            Item vase = new Item("Baroque vase", "A regular baroque vase", 1000, baroque);
            vase.setId(UserFriendlyClient.addItem(vase).getId());
            Item brokenVase = new Item("Broken and glued baroque vase",
                    "I broke my old vase and now it's modern", 10000, modern);
            UserFriendlyClient.changeItem(vase.getId(), brokenVase);
            brokenVase.setId(vase.getId());
            assertEquals(brokenVase, UserFriendlyClient.getItem(brokenVase.getId()));
        } catch (AuctionException ex) {
            ex.printStackTrace();
            fail();
        }
    }

    @Test
    public void testGetById() {
        try {
            baroque.setId(UserFriendlyClient.addCategory(baroque).getId());
            renaissance.setId(UserFriendlyClient.addCategory(renaissance).getId());
            modern.setId(UserFriendlyClient.addCategory(modern).getId());
            postmodern.setId(UserFriendlyClient.addCategory(postmodern).getId());

            assertEquals(baroque, UserFriendlyClient.getCategory(baroque.getId()));
            assertEquals(renaissance, UserFriendlyClient.getCategory(renaissance.getId()));
            assertEquals(modern, UserFriendlyClient.getCategory(modern.getId()));
            assertEquals(postmodern, UserFriendlyClient.getCategory(postmodern.getId()));

            Item vase = new Item("Baroque vase", "A regular baroque vase", 1000, baroque);
            Item addedVase = UserFriendlyClient.addItem(vase);
            Item brokenVase = new Item("Broken and glued baroque vase",
                    "I broke my old vase and now it's modern", 10000, modern);
            Item teapot = new Item("Baroque teapot", "A regular baroque teapot", 500, baroque);
            Item addedTeapot = UserFriendlyClient.addItem(teapot);

            Bid bid1 = new Bid(1001, addedVase, "2WDTRE");
            Bid bid2 = new Bid(1100, addedVase, "@cha55el");

            bid1.setId(UserFriendlyClient.addBid(bid1).getId());
            bid2.setId(UserFriendlyClient.addBid(bid2).getId());

            Bid bid3 = new Bid(550, addedTeapot, "@cha55el");

            bid3.setId(UserFriendlyClient.addBid(bid3).getId());

            UserFriendlyClient.changeItem(addedVase.getId(), brokenVase);
            brokenVase.setId(addedVase.getId());

            Item foundBrokenVase = UserFriendlyClient.getItem(brokenVase.getId());
            assertEquals(brokenVase, foundBrokenVase);

            brokenVase.addBid(bid1);
            brokenVase.addBid(bid2);

            assertTrue(foundBrokenVase.getBids().contains(bid1));
            assertTrue(foundBrokenVase.getBids().contains(bid2));


        } catch (AuctionException ex) {
            fail();
        }
    }

    @Test
    public void testRaisePrice() {
        try {
            baroque.setId(UserFriendlyClient.addCategory(baroque).getId());
            Item vase = new Item("Baroque vase", "A regular baroque vase", 1000, baroque);

            Item addedVase = UserFriendlyClient.addItem(vase);

            Bid bid1 = new Bid(1001, addedVase, "2WDTRE");
            bid1.setId(UserFriendlyClient.addBid(bid1).getId());

            UserFriendlyClient.raiseBidPrice(bid1.getId(), 1150);

            bid1.setPrice(1150);
            assertEquals(bid1, UserFriendlyClient.getBid(bid1.getId()));
        } catch (AuctionException ex) {
            fail();
        }
    }

    @Test
    public void testChangeCategoryTitle() {
        try {
            modern.setId(UserFriendlyClient.addCategory(modern).getId());
            String newTitle = "Modern & Postmodern";
            UserFriendlyClient.changeCategoryTitle(modern.getId(), newTitle);
            Category newCat = UserFriendlyClient.getCategory(modern.getId());
            assertEquals(newTitle, newCat.getTitle());
        } catch (AuctionException ex) {
            fail();
        }
    }

    @Test(expected = AuctionException.class)
    public void testGetWrongId() throws AuctionException {
        baroque.setId(UserFriendlyClient.addCategory(baroque).getId());
        UserFriendlyClient.getCategory(baroque.getId() - 1);
    }

}