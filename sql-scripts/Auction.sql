drop database if exists auction;
create database auction;

use auction;

create table categories(
	id int not null auto_increment primary key,
    title varchar(100)
);

create table items(
	id int not null auto_increment primary key,
    `name` varchar(50) not null, 
    description varchar(200),
    start_price int default 0,
    category int not null,
    
    foreign key (category) references categories(id) on delete cascade
);

create table bids(
	id bigint not null auto_increment primary key,
    item int not null,
    price int not null,
    owner_info varchar(100),
	
    foreign key (item) references items(id) on delete cascade
);