use bookstore;

drop table if exists books;

create table books(
	id int not null auto_increment primary key,
    title varchar(30),
    `year` smallint,
    pages int
);

insert into books values(null, "The Flowers of Evil",1868, 485);
insert into books values(null, "Divine Comedy", 1320, 760);
insert into books values(null, "Faust", 1828, 416);
insert into books values(null, "Roadside picnic", 1972, 147);
insert into books values(null, "Quietly Flows the Don", 1928, 1679);
insert into books values(null, "Mein Kampf", 1926, 357);