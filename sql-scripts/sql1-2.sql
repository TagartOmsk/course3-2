drop database if exists bookstore;
create database bookstore;

use bookstore;

drop table if exists books;

create table books(
	id int not null auto_increment primary key,
    title varchar(30),
    `year` smallint,
    pages int
);

insert into books values(null, 'The Flowers of Evil',1868, 485);
insert into books values(null, 'Divine Comedy', 1320, 760);
insert into books values(null, 'Faust', 1828, 416);
insert into books values(null, 'Roadside picnic', 1972, 147);
insert into books values(null, 'Quietly Flows the Don', 1928, 1679);
insert into books values(null, 'Mein Kampf', 1926, 357);

alter table books add index `year`(`year`);

select * from books;

select * from books limit 3;

select * from books where id < 4;

select * from books where title = 'Faust';

select * from books where title like 'd%';

select * from books where title like '%l';

select * from books where `year` = 1928;

select * from books where `year` between 1500 and 1900;

select * from books where pages between 300 and 500;

select * from books where `year` between 1500 and 1900 and pages between 300 and 500;

# 3rd task finished

update books set pages = 100;

update books set pages = 1000 where pages >= 1000;

update books set pages = 1000 where pages >= 700 and `year` < 1900;

update books set pages = 1000 where pages >= 700 and title like "D%";

# 4th task finished

delete from books where pages > 500;

delete from books where title like "D%";

delete from books where id between 2 and 5;

delete from books where title like "Roadside picnic";

delete from books where title like "Divine comedy" or title like "Faust" or title like "The Flowers of Evil";

delete from books;

# 5th task finished

alter table books add publishing_house varchar(50);

alter table books add binding enum( 'no binding', 'soft', 'hard') default 'no binding';

update books set publishing_house = 'BuKa' where id between 2 and 4;

alter table books change title bookname varchar(30);

select * from books;

rename table books to book_potato;

select * from book_potato;

drop table book_potato;